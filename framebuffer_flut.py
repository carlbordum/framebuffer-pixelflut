import socketserver
import mmap


HEIGHT = 1920
WIDTH = 1080


f = open("/dev/fb0", "r+b")
size = HEIGHT * WIDTH * 4
m = mmap.mmap(f.fileno(), size)


def set_pixel(fb, screen_height, x, y, rgba):
    pos = (x + y * screen_height) * 4
    fb.seek(pos)
    fb.write(rgba)


def _list_to_int(l, base=10):
    result = 0
    for i, n in enumerate(reversed(l)):
        result = n * base**i + result
    return result


class State:
    HEADER_P, HEADER_X, HEADER_SPACE, X, Y, RGBA = range(6)


class ASCII:
    ZERO = ord('0')
    NINE = ord('9')
    SPACE = ord(' ')
    NL = ord('\n')
    a = ord('a')
    f = ord('f')
    P = ord('P')
    X = ord('X')


class PixelFlutParser:
    def __init__(self):
        self.complete = False
        self.state = State.HEADER_P
        self.x = -1
        self.y = -1
        self.rgb = -1
        self._current = []

    @property
    def result(self):
        return self.x, self.y, self.rgb

    def push(self, byte):
        # PX <x> <y> <rrggbb(aa)>\n
        match (self.state, byte):
            case State.HEADER_P, ASCII.P:
                self.state = State.HEADER_X
            case State.HEADER_X, ASCII.X:
                self.state = State.HEADER_SPACE
            case State.HEADER_SPACE, ASCII.SPACE:
                self.state = State.X
            case State.X, n if ASCII.ZERO <= byte <= ASCII.NINE:
                self._current.append(n - ASCII.ZERO)
            case State.X, ASCII.SPACE:
                self.x = _list_to_int(self._current, base=10)
                self._current = []
                self.state = State.Y
            case State.Y, n if ASCII.ZERO <= byte <= ASCII.NINE:
                self._current.append(n - ASCII.ZERO)
            case State.Y, ASCII.SPACE:
                self.y = _list_to_int(self._current, base=10)
                self._current = []
                self.state = State.RGBA
            case State.RGBA, n if ASCII.ZERO <= byte <= ASCII.NINE:
                self._current.append(n - ASCII.ZERO)
            case State.RGBA, n if ASCII.a <= byte <= ASCII.f:
                self._current.append(n - ASCII.a + 10)
            case State.RGBA, ASCII.NL:
                r0, r1, g0, g1, b0, b1 = self._current
                self.rgb = [(r0 << 4) + r1, (g0 << 4) + g1, (b0 << 4) + b1]
                self._current = []
                self.complete = True
            case _:
                raise ValueError()


class MyTCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        parser = PixelFlutParser()
        # self.request is the TCP socket connected to the client
        while True:
            data = self.request.recv(1024)
            for b in data:
                try:
                    parser.push(b)
                except ValueError:
                    print(f"Error in state {parser.state}")
                    return  # close connection
                if parser.complete:
                    x, y, rgb = parser.result
                    parser = PixelFlutParser()
                    argb_little_endian = bytes(rgb[::-1] + [0])
                    set_pixel(m, HEIGHT, x, y, argb_little_endian)


if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 1337
    with socketserver.ForkingTCPServer((HOST, PORT), MyTCPHandler) as server:
        server.serve_forever()
